export default function authHeader() {
    const token = JSON.parse(localStorage.getItem('token'));
    if (token && token.accessToken) {
        return { 
            'x-access-token': token.accessToken,
            'api-key': process.env.REACT_APP_BASE_API_KEY
        };
    } else {
        return {};
    }
}