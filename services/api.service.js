import api from './private-api.service';
import authHeader from './auth-header';
const API_URL = process.env.REACT_APP_BASE_API;

class ApiService {
  getPublicContent() {
    return api.get(API_URL);
  }
  getInfoUser() {
    return api.get(API_URL + '/auth/user', { headers: authHeader() });
  }
}
export default new ApiService();