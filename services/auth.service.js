import axios from "axios";
import { getCookies, getCookie, setCookie, deleteCookie } from 'cookies-next';
import localDataService from "./local.data.service";

const login = async (username, password) => {
  const formData = new FormData();
  formData.append("username", username);
  formData.append("password", password);

  const response = await axios
    .post(process.env.NEXT_PUBLIC_BASE_API + "/auth/login", formData, {
      headers: {
        "Content-Type": "multipart/form-data",
        "Accept": "application/json",
      },
    });
    console.log(response.data);
  if (response?.data?.data?.token) {
    setCookie("token", response.data.data.token, { path: '/' });
    setCookie("user", response.data.data, { path: '/' });
    localDataService.setLocalAccessToken(response.data.data.token);
    localDataService.setUser(response.data.data);
  }
  return response.data;
};
const checkAuth = async () => {
  const response = await axios
    .get(process.env.NEXT_PUBLIC_BASE_API + "/auth/user", {
      headers: {
        "Authorization": "Bearer " + localDataService.getLocalAccessToken(),
        "Accept": "application/json",
      },
    });
  return response.data;
};
const logout = async () => {
  const response = await axios
    .get(process.env.NEXT_PUBLIC_BASE_API + "/auth/logout", {
      headers: {
        "Authorization": "Bearer " + localDataService.getLocalAccessToken(),
        "Accept": "application/json",
      },
    });
    deleteCookie("token", { path: '/' });
    deleteCookie("user", { path: '/' });
    localStorage.removeItem("user");
  return response.data;
};
const getCurrentUser = () => {
  return JSON.parse(localStorage.getItem("user"));
};
const AuthService = {
  login,
  logout,
  checkAuth,
  getCurrentUser,
};
export default AuthService;