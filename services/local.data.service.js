class TokenService {
    getLocalRefreshToken() {
        const user = JSON.parse(localStorage.getItem("token"));
        return user?.refreshToken;
    }
    getLocalAccessToken() {
        const user = JSON.parse(localStorage.getItem("token"));
        return user;
    }
    setLocalAccessToken(token) {
        localStorage.setItem("token", JSON.stringify(token));
    }
    updateLocalAccessToken(token) {
        let user = JSON.parse(localStorage.getItem("token"));
        user.accessToken = token;
        localStorage.setItem("token", JSON.stringify(user));
    }
    getUser() {
        return JSON.parse(localStorage.getItem("user"));
    }
    setUser(user) {
        localStorage.setItem("user", JSON.stringify(user));
    }
    removeUser() {
        localStorage.removeItem("user");
    }
}
export default new TokenService();