This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Application Information

### Engine

| Name     | Version | References                              |
| :------- | :------ | :-------------------------------------- |
| NextJS  | 13.5.4   | https://nextjs.org/                    |
| NodeJS      | v16.20.0     | https://nodejs.org/download/release/v16.20.2/node-v16.20.2-x64.msi |
| React | 18   | https://react.dev/                |
| TailwindCSS | v3.3.3   | https://tailwindcss.com/                |

### Packages

| Name              | Purpose Ï                                   | Repository                                |
| :---------------- | :------------------------------------------ | :---------------------------------------- |
| axios  | axios is a promise-based HTTP Client for node.js and the browser. | https://axios-http.com/docs/intro    |
| framer-motion | Framer Motion is a simple yet powerful motion library for React.                         | https://www.framer.com/motion/introduction/ |
| sweetalert2    | POPUP BOXES | https://sweetalert2.github.io/    |
| fortawesome           | Font Awesome is the Internet's icon library and toolkit                      | https://fontawesome.com/ |

## Prerequisites

-   NodeJS v16
-   ReactJS v18
-   Sass Compiler, [`configuring Sass`](https://nextjs.org/docs/app/building-your-application/styling/sass)


## Getting Started
1. Copy file `.env.example` to file `.env` have been created and please fill this field with your data

    ```.dotenv
    NEXT_PUBLIC_BASE_API=
    ```

2. Install require Package
    ```
    npm install
    ```
3. Install PHP dependencies using Composer :
    ```bash
    npm run dev
    # or
    yarn dev
    # or
    pnpm dev
    # or
    bun dev
    ```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `pages/index.js`. The page auto-updates as you edit the file.

[API routes](https://nextjs.org/docs/api-routes/introduction) can be accessed on [http://localhost:3000/api/hello](http://localhost:3000/api/hello). This endpoint can be edited in `pages/api/hello.js`.

The `pages/api` directory is mapped to `/api/*`. Files in this directory are treated as [API routes](https://nextjs.org/docs/api-routes/introduction) instead of React pages.

This project uses [`next/font`](https://nextjs.org/docs/basic-features/font-optimization) to automatically optimize and load Inter, a custom Google Font.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!
