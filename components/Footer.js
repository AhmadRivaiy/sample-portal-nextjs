import Image from 'next/image'
import { Montserrat } from 'next/font/google'

const montserrat = Montserrat({ subsets: ['latin'] })

export default function Footer({ children }) {
    return (
        <footer className={`mt-auto p-4 text-base-content ${montserrat.className}`}>
            <div className='flex flex-row flex-wrap md:flex-nowrap items-center container mx-auto'>
                <div className='text-center w-full'>
                    <p>Copyright © 2023 Dinas Pendidikan Jawa Barat. All Rights Reserved.</p>
                </div>

                <div className="grid-flow-col md:place-self-center md:justify-self-end w-full">
                    <p className='w-full text-center lg:text-right'>Single Sign On</p>
                </div>
            </div>
        </footer>
    )
}
