import Image from 'next/image'
import { Montserrat } from 'next/font/google'

const montserrat = Montserrat({ subsets: ['latin'] })

export default function MainBody({ children }) {
    return (
        <div className={`c-main-body flex flex-col min-h-screen ${montserrat.className}`}>
            { children }
        </div>
    )
}
