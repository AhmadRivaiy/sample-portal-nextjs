import Image from 'next/image';
import { Montserrat } from 'next/font/google';
import ThemeSwitch from './ThemeSwitch';
import AuthService from '@/services/auth.service';
import { useEffect } from 'react';

const montserrat = Montserrat({ subsets: ['latin'] })

export default function Navbar(props) {
    //CREATE LOGOUT PROCESS
    const processLogout = async () => {
        window.location.href = '/';
    }

    useEffect(() => {
        if(!props?.data){
            window.location.href = '/';
        }    
    }, [props])

    return (
        <div className={`navbar bg-base-100 shadow ${montserrat.className}`}>
            <div className='container mx-auto'>
                <div className="flex-none">
                    <img src='assets/img/disdikjabar.png' className='w-16' />
                </div>
                <div className="flex-1">
                    <a className="btn btn-ghost normal-case text-xl">PLATER</a>
                </div>
                {/* <ThemeSwitch /> */}
                <div className=" font-medium mr-1">
                    {props?.data?.name}
                </div>
                <div className="flex gap-2">
                    <div className="dropdown dropdown-end">
                        <label tabIndex={0} className="btn btn-ghost btn-circle avatar">
                            <div className="w-10 rounded-full">
                                <img src="assets/img/user-286.png" />
                            </div>
                        </label>
                        <ul tabIndex={0} className="mt-3 z-[1] p-2 shadow menu menu-sm dropdown-content bg-base-100 rounded-box w-52">
                            <li><a>Pengaturan</a></li>
                            <li onClick={() => processLogout()} style={{ color: 'red' }}><a>Logout</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    )
}
