import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowLeft, faArrowRight } from "@fortawesome/free-solid-svg-icons";
import React, { useState, useEffect } from 'react';
import {
    StackedCarousel,
    ResponsiveContainer,
} from "react-stacked-center-carousel";

export const data = [
    {
        cover: "https://images6.alphacoders.com/679/thumb-1920-679459.jpg",
        title: "Interstaller",
    },
    {
        cover: "https://images2.alphacoders.com/851/thumb-1920-85182.jpg",
        title: "Inception",
    },
    {
        cover: "https://images6.alphacoders.com/875/thumb-1920-875570.jpg",
        title: "Blade Runner 2049",
    },
    {
        cover: "https://images6.alphacoders.com/114/thumb-1920-1141749.jpg",
        title: "Icon man 3",
    },
    {
        cover: "https://images3.alphacoders.com/948/thumb-1920-948864.jpg",
        title: "Venom",
    },
    {
        cover: "https://images2.alphacoders.com/631/thumb-1920-631095.jpg",
        title: "Steins Gate",
    },
    {
        cover: "https://images4.alphacoders.com/665/thumb-1920-665242.png",
        title: "One Punch Man",
    },
    {
        cover: "https://images2.alphacoders.com/738/thumb-1920-738176.png",
        title: "A Silent Voice",
    },
    {
        cover: "https://images8.alphacoders.com/100/thumb-1920-1005531.jpg",
        title: "Demon Slayer",
    },
    {
        cover: "https://images2.alphacoders.com/582/thumb-1920-582804.png",
        title: "Attack On Titan",
    },
];

export default function ResponsiveCarousel(props) {
    const ref = React.useRef();
    const [count, setCount] = useState(0);
    const [intervalSlide, setIntervalSlide] = useState(props?.interval ?? 3000);

    useEffect(() => {
        // Menjalankan fungsi setiap 3 detik
        const intervalId = setInterval(() => {
            ref.current?.goNext(6);
        }, intervalSlide);

        // Membersihkan interval ketika komponen dilepas
        return () => clearInterval(intervalId);
    }, []);

    const callbackCurrentSlide = (index) => {
        props.callbackcurrent(index);
    }

    return (
        <div style={{ width: "100%", position: "relative" }} className={props?.className}>
            <ResponsiveContainer
                carouselRef={ref}
                render={(parentWidth, carouselRef) => {
                    // If you want to use a ref to call the method of StackedCarousel, you cannot set the ref directly on the carousel component
                    // This is because ResponsiveContainer will not render the carousel before its parent's width is determined
                    // parentWidth is determined after your parent component mounts. Thus if you set the ref directly it will not work since the carousel is not rendered
                    // Thus you need to pass your ref object to the ResponsiveContainer as the carouselRef prop and in your render function you will receive this ref object
                    let currentVisibleSlide = 3;
                    if (parentWidth <= 1440) currentVisibleSlide = 1;
                    if (parentWidth <= 1080) currentVisibleSlide = 1;
                    return (
                        <StackedCarousel
                            ref={carouselRef}
                            slideComponent={Card}
                            slideWidth={parentWidth < 800 ? parentWidth - 40 : 750}
                            carouselWidth={parentWidth}
                            data={props?.data ?? data}
                            fadeDistance={0.2}
                            currentVisibleSlide={currentVisibleSlide}
                            maxVisibleSlide={3}
                            useGrabCursorZ
                            onActiveSlideChange={(index) => callbackCurrentSlide(index)}
                        />
                    );
                }}
            />
            {/* <>
                <button className="btn btn-circle" style={{ position: "absolute", top: "40%", left: 10, zIndex: 10 }}
                    size="small"
                    color="primary"
                    onClick={() => {
                        ref.current?.goBack();
                    }}>
                    <FontAwesomeIcon icon={faArrowLeft} className=" text-black" />
                </button>
                <button className="btn btn-circle"
                    style={{ position: "absolute", top: "40%", right: 10, zIndex: 10 }}
                    size="small"
                    color="primary"
                    onClick={() => {
                        ref.current?.goNext(6);
                    }}>
                    <FontAwesomeIcon icon={faArrowRight} className=" text-black" />
                </button>
            </> */}
        </div>
    );
}

// Very import to memoize your Slide component otherwise there might be performance issue
// At minimum your should do a simple React.memo(SlideComponent)
// If you want the absolute best performance then pass in a custom comparator function like below 
export const Card = React.memo(function (props) {
    const { data, dataIndex, isCenterSlide } = props;
    const { cover } = data[dataIndex];
    return (
        <div
            style={{
                width: "100%",
                height: 400,
                userSelect: "none",
                backgroundColor: "#f6f6f6",
                borderRadius: 20,
                boxShadow: "0px 0px 10px 0px rgba(0,0,0,0.2)",
            }}
            className="flex flex-col justify-center items-center gap-4"
        >
            <div>
                <img
                    style={{
                        height: 250,
                        width: "100%",
                        // objectFit: "cover",
                        borderRadius: 20,
                        // padding: 20,
                    }}
                    draggable={false}
                    src={cover}
                />
            </div>
            {
                isCenterSlide ? (
                    <button className="btn btn-success btn-outline w-max transition ease-in-out">
                        Kunjungi
                    </button>
                ) : <div style={{ height: 50 }}></div>
            }
        </div>
    );
});