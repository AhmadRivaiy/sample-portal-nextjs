import Footer from '@/components/Footer'
import MainBody from '@/components/MainBody'
import { Html, Head, Main, NextScript } from 'next/document'

export default function Document() {
  return (
    <Html lang="en" data-theme="light">
      <Head />
      <body>
        <MainBody>
          <Main />
          <NextScript />
          <Footer />
        </MainBody>
      </body>
    </Html>
  )
}
