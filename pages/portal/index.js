import { motion, AnimatePresence } from 'framer-motion';
import { Montserrat } from 'next/font/google'
import Navbar from '@/components/Navbar'
import ResponsiveCarousel from '@/components/CarouselApp'
import { fetchCurrentUser } from '@/libs/FetchUser';
import { getCookies, getCookie, setCookie, deleteCookie } from 'cookies-next';
import { useState } from 'react'

const montserrat = Montserrat({ subsets: ['latin'] })

export default function Portal(props) {
    const [list, setList] = useState([0, 1, 2, 3, 4, 5,]);
    const [listKategori, setListKategori] = useState([0, 1, 2, 3, 4, 5,]);
    const [currentKategori, setCurrentKategori] = useState(0);
    const [current, setCurrent] = useState(0);
    const [listApp, setListApp] = useState([
        {
            cover: "assets/img/logo-si-gesit.png",
            title: "Sigesit",
            description: `Sigesit Juara merupakan sistem informasi yang
            dikembangkan langsung oleh Bidang GTK Dinas Pendidikan
            Jawa Barat yang terdiri dari 3 lingkup yaitu, sistem
            pendataan, pengelolaan layanan administrasi dan penilaian
            kinerja guru.`
        },
        {
            cover: "assets/img/logo-simantap.png",
            title: "Simantap",
            description: "Menampilkan data agregat jenjang pendidikan SMA, SMK, dan SLB dalam bentuk tabel dan grafik."
        },
        {
            cover: "assets/img/logo-jfls.png",
            title: "JFLS",
            description: "Merupakan program bantuan biaya pendidikan tinggi dari Pemerintah Provinsi Jawa Barat melalui Dinas Pendidikan untuk masyarakat Jawa Barat yang sedang menempuh pendidikan jenjang D3, D4, S1, S2, atau S3 yang berprestasi di bidang akademik maupun non-akademik."
        },
        {
            cover: "assets/img/logo-ppdb.png",
            title: "PPDB",
            description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi dapibus a erat rutrum gravida. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae."
        },
        {
            cover: "assets/img/logo-sicakap.png",
            title: "Sicakap",
            description: "Sistem Informasi Calon Kepala Sekolah (SICAKAP) dalam proses seleksi sebagai bentuk transparansi. SICAKAP menjadi salah satu inovasi dalam proses seleksi BCKS SMA/SMK/SLB sekaligus untuk menjaga independensi."
        },
        {
            cover: "assets/img/logo-pusaka.svg",
            title: "Pusaka",
            description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi dapibus a erat rutrum gravida. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae."
        },
        {
            cover: "assets/img/logo-default-no.png",
            title: "PKLK Juara",
            description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi dapibus a erat rutrum gravida. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae."
        },
        {
            cover: "assets/img/logo-default-no.png",
            title: "Si Mantan",
            description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi dapibus a erat rutrum gravida. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae."
        }
    ]);

    return (
        <main className={` ${montserrat.className}`}>
            <Navbar data={{name: 'Sample User'}} />
            <div className=''>
                <div className='container mx-auto'>
                    <div className='font-bold text-center sm:text-5xl text-3xl mt-10 transition-opacity duration-500 opacity-100'>
                        {/* <AnimatePresence mode="wait" initial={false}>
                            <motion.div
                                key={"app" + current}
                                initial={{ opacity: 0 }}
                                animate={{ opacity: 1 }}
                                exit={{ opacity: 0 }}
                                transition={{ duration: 0.2 }}
                            >
                                {listApp[current]?.title}
                            </motion.div>
                        </AnimatePresence> */}

                        {listApp[current]?.title}
                    </div>
                    <div className='font-medium text-center text-1xl md:text-2xl mt-10 px-10 h-32'>{listApp[current]?.description != "" ? listApp[current]?.description : 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi dapibus a erat rutrum gravida. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae.'}</div>
                </div>
                <ResponsiveCarousel
                    className="mt-14"
                    interval={5000}
                    callbackcurrent={(e) => setCurrent(e)}
                    data={listApp}
                />
                <div className='container mx-auto'>
                    <div className='font-bold text-center sm:text-5xl text-3xl mt-10 mb-10'>{'Kategori'}</div>
                    <div className="grid grid-flow-row-dense grid-cols-2 grid-rows-2 gap-8 md:grid-cols-2 lg:grid-cols-6 mb-10">
                        {
                            listKategori.map((item, index) => {
                                return (
                                    <div className='flex justify-center' key={index}>
                                        <button className={`btn btn-neutral btn-ghost ${currentKategori == index ? `active` : ``} current:bg-blue-400 current:text-white`} onClick={() => setCurrentKategori(index)}>
                                            Semua
                                        </button>
                                    </div>
                                )
                            })
                        }
                    </div>
                    <div className="grid grid-flow-row-dense grid-cols-1 grid-rows-2 gap-8 md:grid-cols-2 lg:grid-cols-3 mb-10">
                        {
                            listApp?.map((item, index) => {
                                return (
                                    <div className='flex justify-center' key={index}>
                                        <div className="card w-96 bg-base-100 shadow-xl">
                                            <figure className="px-10 pt-10">
                                                <img src={item?.cover} alt="Shoes" className="rounded-xl p-5"/>
                                            </figure>
                                            <div className="card-body items-center text-center">
                                                <h2 className="card-title">{item?.title}</h2>
                                                <p>{item?.description}</p>
                                                <div className="card-actions">
                                                    <button className="btn btn-success btn-outline w-max transition ease-in-out">
                                                        Kunjungi
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                )
                            })
                        }
                    </div>
                </div>
            </div>
        </main>
    )
};