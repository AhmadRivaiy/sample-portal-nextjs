import Image from 'next/image'
import { Montserrat } from 'next/font/google';
import { getCookies, getCookie, setCookie, deleteCookie } from 'cookies-next';
import { useEffect, useState } from 'react'
import AuthService from '@/services/auth.service'
import Swal from 'sweetalert2'
import localDataService from '@/services/local.data.service'
import { fetchCurrentUser } from '@/libs/FetchUser';

const montserrat = Montserrat({ subsets: ['latin'] })

export default function Login() {
  const [isLogined, setIsLogined] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    if (isLogined) {
      // IF LOGINED
      window.location.href = '/portal';
    }
  }, [isLogined]);

  const processLogin = async (e) => {
    e.preventDefault();

    //Check Username and Password to Database
    console.log(e.target.username.value, e.target.password.value);
    
    Swal.fire({
      title: 'Success!',
      text: 'Login Success!',
      icon: 'success',
      confirmButtonText: 'Ok'
    }).then((result) => {
      setIsLogined(true);
    })
  }

  return (
    <main
      className={`${montserrat.className}`}
    >
      <div className="flex flex-col items-center justify-center container-login">
        <div>
          <div className="w-fit container-form flex flex-row">
            <div className="header w-fit mb-0 md:mb-12">
              <img src="assets/img/disdikjabar.png" alt="" />
              <br/>
              <h2>Login SSO</h2>
              <br/><br/>
              <p className='w-full'>Satu Login Untuk Semua Aplikasi Dinas Pendidikan Provinsi Jawa Barat</p>
            </div>
          </div>

          <div className="flex flex-row">
            <form onSubmit={processLogin} className='w-fit'>
              <label htmlFor="">Username/NIK/NIP/NISN</label>
              <br />
              <input type="text" name="username" placeholder="Username" required />
              <br />
              <label htmlFor="" className="lb2">Password</label>
              <br />
              <input type="password" autoComplete='current-password' name="password" placeholder="Text placeholder" className="inp2" required />
              <br />
              <button className="btn-login" type='submit'>Login</button>
              <br />
              <a href="">Lupa Password ?</a>
            </form>
            <img className=' hidden md:block pb-14' src="assets/img/undraw_Software_engineer_re_tnjc 1.png" alt="" width={400} height={100} />
          </div>
        </div>
      </div>
    </main>
  )
}