// VIEW 500 ERROR SERVER
export default function Custom500() {
    return (
        <>
            <main className="bg-gray-100 h-screen">
                <div className="flex flex-col items-center justify-center h-full">
                    <h1 className="text-6xl font-bold">500</h1>
                    <h2 className="text-2xl font-bold">Internal Server Error</h2>
                    <p className="text-xl text-gray-600">Something went wrong. Please try again later.</p>
                </div>
            </main>
        </>
    )
}