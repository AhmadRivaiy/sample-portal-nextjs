import '@/styles/globals.css'
import '@/styles/globals-with-scss.scss'

import React from 'react'
import Head from 'next/head'
 
function MyApp({ Component, pageProps }) {
  return (
    <>
      <Head>
        <title>PLATER Disdik</title>
      </Head>
      <Component {...pageProps} />
    </>
  )
}
 
export default MyApp