// VIEW 404 ERROR SERVER
export default function Custom404() {
    return (
        <>
            <main className="bg-gray-100 h-screen">
                <div className="flex flex-col items-center justify-center h-full">
                    <h1 className="text-6xl font-bold">404</h1>
                    <br/>
                    <h2 className="text-2xl font-bold">Not Found</h2>
                    <br/>
                    <a href="/" className="text-xl text-gray-600"><button className="btn btn-primary btn-outline w-max transition ease-in-out">
                        Kembali
                    </button></a>
                </div>
            </main>
        </>
    )
}