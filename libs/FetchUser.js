import axios from "axios";

export async function fetchCurrentUser(token, context, params) {
    const props = {
        data: null,
        status: 200,
        message: ''
    }

    if (!token) {
        props.status = 401;
    }

    await axios.request({
        url: process.env.NEXT_PUBLIC_BASE_API + '/auth/user',
        method: 'GET',
        headers: { 'Accept': 'application/json', 'Authorization': 'Bearer ' + token }
    })
        .then(x => {
            props.data = x.data.data;
            props.message = x.data.message;
            props.status = 200;
        })
        .catch(err => {
            if (err) {
                if (err?.response?.status === 400) {
                    props.status = 400;
                }
                if (err?.response?.status === 401) {
                    props.status = 401;
                }
                if (err?.response?.status === 403) {
                    props.status = 403;
                }
                if (err?.response?.status === 404) {
                    props.status = 404;
                }
                if (err?.response?.status === 500) {
                    props.status = 500;
                }
            }
        });

    return props;
}